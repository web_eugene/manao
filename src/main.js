import { ViteSSG } from "vite-ssg/single-page";
import App from "@/App.vue";
import "./assets/scss/base.scss";

export const createApp = ViteSSG(App);

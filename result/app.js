const swiper = new Swiper(".swiper", {
  loop: true,
  slidesPerView: 1,
  spaceBetween: 0,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  breakpoints: {
    580: {
      slidesPerView: 3,
      spaceBetween: 30,
    },
  },
});

const rules = {
  required: (val) => !!val || "Поле обязательно",
  email: (val) =>
    /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,16})+$/.test(val) ||
    "E-mail не корректен",
};

const fields = {
  name: rules.required,
  email: rules.email,
};

const validateHandler = (event) => {
  const target = event.target || event;
  const value = target.value.trim();
  const name = target.getAttribute("name");
  const errorMessageNode =
    target.parentElement.querySelector(".m-input__error");
  const isValidMessage = fields[name](value);

  if (typeof isValidMessage === "string") {
    target.classList.add("error");
    errorMessageNode.innerHTML = isValidMessage;
    return false;
  } else {
    target.classList.remove("error");
    errorMessageNode.innerHTML = "";
    return true;
  }
};

Object.keys(fields).forEach((filedName) => {
  const input = document.querySelector(`.m-input__input[name=${filedName}]`);
  input.addEventListener("blur", validateHandler);
});

const form = document.querySelector(".m-footer__form");

const popup = document.querySelector(".m-popup");
form.addEventListener("submit", (e) => {
  e.preventDefault();

  const isNotValid = !!Object.keys(fields)
    .map((filedName) => {
      const input = document.querySelector(
        `.m-input__input[name=${filedName}]`
      );
      return validateHandler(input);
    })
    .filter((val) => !val).length;

  const formData = new FormData(e.target);
  const formProps = Object.fromEntries(formData);

  if (!isNotValid) {
    // Send formProps
    popup.classList.add("open");
  }
});
document
  .querySelector(".m-popup__close, .m-popup__bg")
  .addEventListener("click", () => popup.classList.remove("open"));

const header = document.querySelector(".m-header");
const burger = document.querySelector(".m-header__burger");

burger.addEventListener("click", () => {
  burger.classList.toggle("active");
  header.classList.toggle("active");
});

header.querySelectorAll(".m-header__menu-list__item").forEach((link) => {
  link.addEventListener("click", (e) => {
    burger.classList.remove("active");
    header.classList.remove("active");
  });
});
